<?php

class Player
{
    public $inventory;

    function __construct()
    {
        $this->inventory = [];
    }



    public function getInventory()
    {
        return $this->inventory;
    }

    public function setInventory($inventory)
    {
        $this->inventory[] = $inventory;
    }
    public function removeInventory($inventory)
    {
        unset($this->inventory[$inventory]);
    }


}