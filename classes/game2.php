<?php

class Game2
{
    private $map;
    private $player;

    function __construct($player)
    {
        $this->map = [];
        $this->player = $player;
    }

    public function getMap()
    {
        return $this->map;
    }

    public function setMap($map)
    {
        $this->map = $map;
    }


    public $mapP;

    function __constructP()
    {
        $this->mapP = [];
    }

    public function getMapP()
    {
        return $this->mapP;
    }

    public function setMapP($mapP)
    {
        $this->mapP = $mapP;
    }
    public function setCell($line, $cell, $value)
    {
        $this->mapP[$line][$cell] = $value;
    }
    public function getCell($line, $cell)
    {
        if ($line>=0 AND $cell>=0 AND $line<count($this->mapP) AND $cell<count($this->mapP[0])) {
            return $this->mapP[$line][$cell];
        }

    }







    public function init() {
        $curMap = [
            [0,0,1,0,'k'],
            [0,1,1,0,0],
            ['s',1,0,0,1],
            [0,1,0,0,'e'],
            [0,0,1,0,0]
        ];

        $this -> setMap($curMap);
        $this -> setMapP($curMap);
        for($line = 0; $line < count($this->getMapP()); $line++){
            for($cell = 0; $cell < count($this->getMapP()[$line]); $cell++){
                if ($this->getMapP()[$line][$cell] == 's') {
                    $this->setCell($line,$cell,'p');
                    $playerPos = [$line,$cell];
                }
            }
            echo "\n";
        }
        function showMap($map)
        {
//            var_dump($map);
            echo '| ';
            for ($column = 0; $column < count($map[0]); $column++){

                echo " - ";
            }
            echo ' |';
            echo "\n";
            for($line = 0; $line < count($map); $line++){
                echo '| ';
                for($cell = 0; $cell < count($map[$line]); $cell++){
                    echo ' '.$map[$line][$cell].' ';
                }
                echo ' |';
                echo "\n";
            }
            echo '| ';
            for ($column = 0; $column < count($map[0]); $column++){
                echo " - ";
            }
            echo ' |';
            echo "\n";
        }
        function pickDir($dir)
        {
            if ($dir == 'haut') {
                return [-1,0];
            } elseif ($dir == 'droite') {
                return [0,1];
            }
            elseif ($dir == 'gauche') {
                return [0,-1];
            }
            elseif ($dir == 'bas') {
                return [1,0];
            }
        }
        showMap($this->getMapP());
        $won1 = 0;
        while ($won1 == 0) {
            $inputDir = 0;
            while($inputDir != 'haut' AND $inputDir != 'bas' AND $inputDir != 'droite' AND $inputDir != 'gauche') {
                echo 'Quelle direction?';
                echo "\n";
                $inputDir =rtrim(fgets(STDIN));
                pickDir($inputDir);
                $moveDir= pickDir($inputDir);
            }
            $playerPosNew = [$playerPos[0]+$moveDir[0],$playerPos[1]+$moveDir[1]];
            if ($this->getCell($playerPosNew[0],$playerPosNew[1]) == 1
                OR $this->getCell($playerPosNew[0],$playerPosNew[1]) === null
                OR ($this->getCell($playerPosNew[0],$playerPosNew[1]) == 'e' AND in_array('cle', $this->player->getInventory()) == 0) ) {
                echo 'impossible';
                if ($this->getCell($playerPosNew[0],$playerPosNew[1]) == 'e') {
                    echo ', vous n\'avez pas la clé.';
                }
                echo "\n";
            } else {
                if ($this->getCell($playerPosNew[0],$playerPosNew[1]) == 'k') {
                    $this->player->setInventory('cle');
                    var_dump($this->player->getInventory());
                    echo 'vous avez obtenu la clé de la sortie';
                    echo "\n";
                }
                if ($this->getCell($playerPosNew[0],$playerPosNew[1]) == 'e' AND in_array('cle', $this->player->getInventory()) == 1) {
                    $won = 1;
                    echo 'Vous avez atteint la sortie, felicitation!';
                }
                $this->setCell($playerPosNew[0],$playerPosNew[1],'p');
                $this->setCell($playerPos[0],$playerPos[1],0);
                $playerPos= $playerPosNew;
            }
            if ($won1 == 0) {
                showMap($this->getMapP());
            }
        }
    }
}